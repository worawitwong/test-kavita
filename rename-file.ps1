$currentLocation = Get-Location
Get-ChildItem $currentLocation -Recurse -Include *.epub |
ForEach-Object {
 $_ | Rename-Item -NewName { $_.Name -replace ' ','_'}
 $_ | Rename-ItEM -NewName { $_.Name -replace "'",'_'}
}